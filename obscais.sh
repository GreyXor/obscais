#!/bin/zsh

# Function to check if the host has an active internet connection
check_internet() {
	# Print a checking messages
	echo "Checking internet connection.."

	# Use curl to check if the host can reach the Arch Linux website
	# -sL: Silent mode (suppressing progress meter) and follow redirects (L)
	# -w '%{http_code}': Print the HTTP status code returned by the server
	# -o /dev/null: Redirect the actual response body to /dev/null, since we only care about the status code

	# If the status code returned is not 200 (OK), the host is not connected to the internet
	if [ ! "$(curl -sL -w '%{http_code}' https://ping.archlinux.org/nm-check.txt -o /dev/null)" = "200" ]; then
		# Print a message indicating that the host is not connected to the internet
		echo "System is not connected to internet."

		# Exit the script with a status code of 1 to indicate an error (in this case, no internet)
		exit 1
	fi

	# Print a success message
	echo "Internet connection works"
}

# Function to check if a profile directory is valid and contains all necessary files
check_profile() {
	# Store the path to the profile directory in a variable for ease of reference
	profile_directory="$1"

	# Display a message to indicate which profile directory is being checked
	echo "Checking profile located in $profile_directory"

	# Define an array of required files and directories that should be present
	required_files=(
		"$profile_directory"                                         # The profile directory itself should exist
		"$profile_directory/configuration.sh"                        # The configuration file that contains the installation parameters
		"$profile_directory/rootfs/boot/loader/entries"              # Directory containing boot loader entries
		"$profile_directory/rootfs/boot/loader/loader.conf"          # The loader configuration file
		"$profile_directory/rootfs/etc/xdg/reflector/reflector.conf" # Reflector configuration file
		"$profile_directory/rootfs/etc/hostname"                     # Hostname configuration file
		"$profile_directory/rootfs/etc/locale.conf"                  # Locale configuration file
		"$profile_directory/rootfs/etc/locale.gen"                   # Locale generation file
		"$profile_directory/rootfs/etc/vconsole.conf"                # Virtual console configuration file
		"$profile_directory/homefs"                                  # The homefs directory (this can be empty, no check for non-emptiness)
	)

	# Loop through all the required files and directories to verify their existence and validity
	for file in "${required_files[@]}"; do
		# Check if the file or directory does not exist
		if [[ ! -e "$file" ]]; then
			# If the file/directory is missing, print an error and exit the function
			echo "Error: Missing required file/directory: $file"
			exit 1
		fi

		# Check if the file is empty (skip the check for homefs)
		if [[ -f "$file" && ! -s "$file" && "$file" != "$profile_directory/homefs" ]]; then
			# If it's a file and it's empty, print an error message
			echo "Error: File is empty: $file"
			exit 1
		fi

		# Check if the directory is empty (skip homefs directory)
		if [[ -d "$file" && "$file" != "$profile_directory/homefs" && -z "$(find "$file" -type f 2>/dev/null)" ]]; then
			# If it's a directory and it contains no files, print an error message
			echo "Error: Directory is empty: $file"
			exit 1
		fi
	done

	# Check that required variables exist in the configuration.sh file and are not empty
	source "$profile_directory/configuration.sh"

	# List of variables to check that exist in the profile configuration
	required_variables=(
		"OBS_CLEAN_EFI"
		"OBS_DEVICE"
		"OBS_DEVICE_NVME_FORMAT_LBAF"
		"OBS_DEVICE_NVME_FORMAT_SES"
		"OBS_DEVICE_PART_BOOT_SIZE"
		"OBS_DEVICE_PART_SWAP_SIZE"
		"OBS_DEVICE_PART_ROOT_SIZE"
		"OBS_DEVICE_PART_HOME_SIZE"
		"OBS_TIMEZONE"
		"OBS_HOMED_USERNAME"
	)

	# Loop through each required variable and check if it exists and is not empty
	for var in "${required_variables[@]}"; do
		# Use eval to expand the variable
		value=$(eval echo \$$var)

		# Check if the variable is empty
		if [[ -z "$value" ]]; then
			echo "Error: $var is not defined or is empty in configuration.sh"
			exit 1
		fi
	done

	# If all checks passed, print a success message
	echo "Profile in $profile_directory appears to be correct."
}

# The `install` function automates the Arch Linux installation process by using variables
# defined in the `configuration.sh` file located in the specified profile directory.
# This is essentially an automated version of the Arch Linux Installation Guide.
install() {
	# Store the path to the profile directory in a variable for ease of reference
	# The profile directory contains all necessary configurations for the installation process,
	# such as partition sizes, packages to install, and locale settings.
	profile_directory="$1"

	# Perform initial checks to ensure prerequisites are met:
	# - Verify that the profile directory exists and is accessible.
	# - Check that the system has an active internet connection.
	check_profile $profile_directory
	# https://wiki.archlinux.org/title/Installation_guide#Connect_to_the_internet
	check_internet

	# Load the configuration file from the profile directory.
	# This file defines various variables required for the installation, such as
	# disk settings, locale configurations, and additional packages.
	source "$profile_directory/configuration.sh"

	# Extract the keyboard layout from the profile's `vconsole.conf` file.
	# The keyboard layout ensures that users enter passwords with the correct key mappings.
	VCONSOLE_TTY_KEYMAP=$(grep '^KEYMAP=' "$profile_directory/rootfs/etc/vconsole.conf" | cut -d'=' -f2)

	# Apply the extracted keyboard layout for consistent input during installation.
	loadkeys "$VCONSOLE_TTY_KEYMAP"

	# Clean up EFI entries if specified in the configuration.
	# This removes duplicate or outdated EFI boot entries to avoid conflicts during boot.
	if [[ "$OBS_CLEAN_EFI" == "true" ]]; then
		efibootmgr --remove-dups
		for bootentry in $(efibootmgr | grep --only-matching --perl-regexp '^Boot\K\d{4}'); do
			efibootmgr --bootnum $bootentry --delete-bootnum
		done
		efibootmgr --delete-bootorder
		efibootmgr --delete-timeout
		efibootmgr --delete-bootnext
	fi

	# Prepare the installation disk by securely erasing and resetting its partition table.
	# This ensures a clean start with no residual data or configurations.
	sync
	shred --force --iterations=1 --random-source=/dev/zero --size=8M --verbose "$OBS_DEVICE"
	sync
	nvme format "$OBS_DEVICE" --lbaf="$OBS_DEVICE_NVME_FORMAT_LBAF" --ses="$OBS_DEVICE_NVME_FORMAT_SES" --force --verbose --reset
	sync

	# Partition the disk as per the configuration file:
	# - Boot partition: for EFI bootloader.
	# - Swap partition: for memory swapping.
	# - Root partition: main system installation.
	# - Home partition: user data and settings.
	# https://wiki.archlinux.org/title/Installation_guide#Partition_the_disks
	sgdisk --new 0:0:"$OBS_DEVICE_PART_BOOT_SIZE" --typecode 0:ef00 --change-name 0:boot --align-end "$OBS_DEVICE"
	sgdisk --new 0:0:"$OBS_DEVICE_PART_SWAP_SIZE" --typecode 0:8200 --change-name 0:swap --align-end "$OBS_DEVICE"
	sgdisk --new 0:0:"$OBS_DEVICE_PART_ROOT_SIZE" --typecode 0:8304 --change-name 0:root --align-end "$OBS_DEVICE"
	sgdisk --new 0:0:"$OBS_DEVICE_PART_HOME_SIZE" --typecode 0:8302 --change-name 0:home --align-end "$OBS_DEVICE"
	sync
	partprobe "$OBS_DEVICE"

	# Format each partition with the specified file systems:
	# - Boot: FAT32 for EFI compatibility.
	# - Swap: activated as swap space.
	# - Root: BTRFS
	# - Home: BTRFS
	# https://wiki.archlinux.org/title/Installation_guide#Format_the_partitions
	mkfs.fat -F 32 -n boot -v "${OBS_DEVICE}p1"
	mkswap --label swap --verbose "${OBS_DEVICE}p2"
	mkfs.btrfs --force --verbose --label root "${OBS_DEVICE}p3"
	mkfs.btrfs --force --verbose --label home "${OBS_DEVICE}p4"
	sync

	# Mount the formatted partitions to prepare for installation:
	# - Root is mounted to `/mnt`.
	# - Home is mounted to `/mnt/home`.
	# - Boot is mounted to `/mnt/boot`.
	# https://wiki.archlinux.org/title/Installation_guide#Mount_the_file_systems
	mount --verbose --options compress=zstd "${OBS_DEVICE}p3" /mnt
	mount --verbose "${OBS_DEVICE}p4" --mkdir /mnt/home
	mount --verbose "${OBS_DEVICE}p1" --mkdir /mnt/boot
	swapon --verbose "${OBS_DEVICE}p2"

	# Configure mirrors for package downloads using the profile's reflector configuration.
	# https://wiki.archlinux.org/title/Installation_guide#Select_the_mirrors
	reflector "@$profile_directory/rootfs/etc/xdg/reflector/reflector.conf"

	# Install the Arch Linux base system and additional packages defined in the configuration.
	# This includes essential tools like the Linux kernel, network utilities, and text editors.
	# https://wiki.archlinux.org/title/Installation_guide#Install_essential_packages
	sync
	pacstrap -K /mnt base base-devel linux linux-firmware dosfstools btrfs-progs man-db man-pages helix networkmanager openssh sudo xdg-user-dirs zsh git reflector $OBS_PACKAGES_TO_INSTALL
	sync

	# Generate the fstab file to map partitions for the installed system.
	# https://wiki.archlinux.org/title/Installation_guide#Fstab
	genfstab -U /mnt >>/mnt/etc/fstab

	# Configure the system's time zone and hardware clock.
	# https://wiki.archlinux.org/title/Installation_guide#Time
	arch-chroot /mnt ln -sf "/usr/share/zoneinfo/$OBS_TIMEZONE" /etc/localtime
	arch-chroot /mnt hwclock --systohc

	# Copy configuration files from the profile directory to the installed system.
	cp -r "$profile_directory/rootfs/"* /mnt

	# Generate the locale settings for the installed system.
	# https://wiki.archlinux.org/title/Installation_guide#Localization
	arch-chroot /mnt locale-gen

	# Configure the hostname resolution for the installed system. (https://wiki.archlinux.org/title/Network_configuration#localhost_is_resolved_over_the_network)
	# https://wiki.archlinux.org/title/Installation_guide#Network_configuration
	printf "127.0.0.1        localhost\n" >/mnt/etc/hosts
	printf "::1              localhost\n" >>/mnt/etc/hosts
	printf "127.0.1.1        %s\n" "$(cat /mnt/etc/hostname)" >>/mnt/etc/hosts

	# Prompt the user to set a password for the root account.
	# https://wiki.archlinux.org/title/Installation_guide#Root_password
	echo "Set the root password (keyboard layout is $VCONSOLE_TTY_KEYMAP)"
	arch-chroot /mnt passwd

	# Install the EFI boot loader for the installed system.
	# https://wiki.archlinux.org/title/Installation_guide#Boot_loader
	arch-chroot /mnt bootctl install

	# Grant sudo privileges to users in the "wheel" group.
	echo "%wheel ALL=(ALL:ALL) ALL" >/mnt/etc/sudoers.d/obs_wheel_all

	# Copy additional scripts for post-install configuration to the installed system.
	cp -r obscais.sh "$profile_directory/configuration.sh" "$profile_directory/homefs" /mnt/root

	# Unmount all mounted partitions and clean up.
	sync
	umount -R /mnt

	# Inform the user that the installation is complete and provide next steps.
	echo "=== $0 finished. Reboot, log as root and run ~/obscais.sh setup_root ==="
}

# The `setup_root` function sets up essential services, configures the home directory for the main user,
# and prepares the system for user-specific configurations. It is run as root and performs critical post-installation steps.
setup_root() {
	# Load the configuration file to access variables and settings for root-level setup.
	source configuration.sh

	# Enable and start essential system services:
	# - `systemd-homed`: Manages home directories as portable containers.
	# - `NetworkManager`: Provides network configuration and connectivity.
	# - `reflector.timer`: Keeps the mirror list updated automatically.
	# - Additional services specified in the configuration file ($OBS_ROOT_SERVICES).
	systemctl enable --now systemd-homed NetworkManager reflector.timer systemd-boot-update $OBS_ROOT_SERVICES

	# Move the installation scripts and configuration files to the home directory skeleton (`homefs`).
	# The skeleton serves as the template for creating the new user's home directory.
	mv obscais.sh configuration.sh homefs/
	touch homefs/.zshrc

	# Synchronize file system changes to disk.
	sync

	# Create the home directory for the user defined in the configuration file:
	# - Use `homectl` to create the home directory based on the `homefs` skeleton.
	# - Set the default shell to Zsh (`/usr/bin/zsh`).
	# - Add the user to the "wheel" group and other groups specified in the configuration file ($OBS_HOMED_USER_GROUPS).
	# - Disable auto-resize mode for the home directory container.
	homectl create "$OBS_HOMED_USERNAME" --skel="homefs" --shell=/usr/bin/zsh --member-of="wheel,$OBS_HOMED_USER_GROUPS"
	sync

	# Clean up the home directory skeleton (`homefs`) as it is no longer needed.
	rm -rf homefs

	# Change the default shell for the root user to Zsh.
	chsh --shell /usr/bin/zsh

	# Notify the user that the root setup is complete and provide the next steps for user setup.
	echo "=== $0 finished. Log as $OBS_HOMED_USERNAME and run ~/obscais.sh setup_user ==="
}

# The `setup_user` function is designed to finalize the user-specific setup
# of the installed Arch Linux system. It assumes the installation is complete
# and should be run as the user defined in the configuration file.
setup_user() {
	# Load the configuration file to access user-specific variables and scripts.
	source configuration.sh

	# Perform initial checks:
	# - Ensure the function is not run as the root user but as the specified username.
	# - Verify internet connectivity for package updates and installations.
	if [[ ! "$EUID" -ne 0 ]]; then
		echo "You have to run this command as $OBS_HOMED_USERNAME user"
		exit 1
	fi
	check_internet

	# Update the package database and upgrade all installed packages to the latest version.
	sudo pacman -Syyuu --noconfirm
	sync

	# Install the AUR helper `paru`:
	# - Clone the `paru` repository from AUR to a temporary directory.
	# - Build and install `paru` for managing AUR packages.
	(git clone --depth=1 https://aur.archlinux.org/paru.git /tmp/paru && cd /tmp/paru && makepkg -si --noconfirm)

	# Initialize and update `paru` for managing the AUR package database.
	paru -Syu --noconfirm && sync && paru --gendb && sync

	# Install additional AUR packages specified in the configuration file.
	paru -S --noconfirm --skipreview $OBS_AUR_PACKAGES

	# Clean up unnecessary files used during the setup process.
	sync
	sudo rm -rf configuration.sh obscais.sh .gitkeep .zshrc /root/.bash_history

	# Execute the user script defined in the configuration file for any custom setup tasks.
	eval "$OBS_POSTUSER_SCRIPT"

	# Notify the user that the setup is complete and provide next steps.
	echo "=== $0 finished. You can reboot and enjoy your Arch Linux. ==="
}

# The `clean` function is responsible for resetting the system to a clean state
# before beginning a new installation process. It unmounts and cleans up
# any partitions used during the installation.
clean() {
	# Store the path to the profile directory in a variable for ease of reference.
	# The profile directory contains the configuration required for cleaning up the installation environment.
	profile_directory="$1"

	# Perform initial checks to ensure the profile directory exists and is accessible.
	check_profile $profile_directory

	# Load the configuration file to access variables required for the cleanup process.
	source "$profile_directory/configuration.sh"

	# Reset the state of the installation environment:
	# - Refresh the partition table to ensure it is up to date.
	# - Turn off swap space to free the swap partition.
	# - Unmount all partitions forcefully and recursively to release resources.
	# - Refresh the partition table again after unmounting.
	partprobe
	swapoff "${OBS_DEVICE}p2"
	umount --force --recursive /mnt
	umount --force --recursive "$OBS_DEVICE"
	partprobe

	# Notify the user that the system is ready for a new installation.
	echo "=== $0 finished. Ready to install ==="
}

# The `help` function displays an informational message about the script, including requirements, commands, and links to documentation.

help() {
	# Use a here-document to print the help message for the user.
	cat <<EOF
-----------------------
OBSCAIS: Opinionated But Somewhat Customizable Arch Install Script
https://gitlab.com/greyxor/obscais
-----------------------

Help and documentation are available in README.md.

Commands are:
[install profile_path]     # Perform an automated installation using the specified profile.
[setup_root]               # Configure essential services and set up the root environment.
[setup_user]               # Finalize the system setup for the user specified in the configuration.
[clean profile_path]       # Clean and reset the system for a fresh installation using the given profile.
[check_profile profile_path] # Validate the structure and contents of the provided profile directory.
[check_internet]           # Verify internet connectivity.
[help]                     # Display this help message.
EOF
}

# Ensure the system is booted in UEFI mode, as this script requires UEFI for installation.
# If not in UEFI mode, terminate with an error message.
# https://wiki.archlinux.org/title/Installation_guide#Verify_the_boot_mode
[[ $(cat /sys/firmware/efi/fw_platform_size 2>/dev/null) == "64" ]] || {
	echo "System is not booted in UEFI mode."
	exit 1
}

# Store the first and second arguments passed to the script in variables for better readability.
command="$1"           # The command to execute (e.g., "install", "setup_root", etc.).
profile_directory="$2" # The path to the profile directory, required by certain commands.

# Check if a command was provided. If a command is given, execute the corresponding function.
# For commands that require a profile directory, ensure the directory path is also provided.
if [ "$command" ]; then
	# Validate that commands needing a profile directory have a path specified.
	if [[ ("$command" == "install" || "$command" == "check_profile" || "$command" == "clean") && -z "$profile_directory" ]]; then
		echo "To use the $command command, you must enter the path to a profile (containing configuration.sh, rootfs, and homefs)."
		exit 1
	fi
	# Synchronize file system changes to disk before and after executing the command.
	sync
	"$command" "$profile_directory"
	sync
else
	# If no command is provided, display the help message.
	help
fi
