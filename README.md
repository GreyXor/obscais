# OBSCAIS
*Opinionated But Somewhat Customizable Arch Install Script*

OBSCAIS streamlines the installation of Arch Linux, staying close to the [official installation guide](https://wiki.archlinux.org/title/installation_guide) while introducing some opinionated configurations. Designed for users who want a fast, automated, and customizable Arch installation, OBSCAIS simplifies many steps without compromising flexibility.

## Key Features of OBSCAIS

- **Official Arch Installation Guide-Based**: Follows the official Arch Wiki installation steps closely, making it a familiar process for experienced users.
- **Two-Reboot, Fully Automated Installation**: The installation process requires only two reboots—one for setting up the base system and another for finishing the setup, reducing manual intervention.
- **Home Directory Encryption**: Integrates `systemd-homed` to encrypt user home directories, enhancing data security by default.
- **AUR Integration with Paru**: Automatically installs `paru`, allowing seamless management of Arch User Repository (AUR) packages alongside official Arch repositories.
- **Optimized Mirror Selection**: Uses `Reflector` to automatically select and sync the fastest package mirrors for your region, ensuring faster downloads during installation.
- **NVMe and Modern Storage Optimization**: Specifically tailored for NVMe drives with GPT and BTRFS, ensuring the best performance for modern storage devices.
- **Sudo and Wheel Group by Default**: The `wheel` group is granted the ability to use `sudo`, simplifying administrative tasks without compromising security.
- **Customizable Profiles**: Create and use custom profiles to define specific partition layouts, installed packages, system settings, and more. Profiles ensure that the installation process is automated, reusable, and adaptable to your needs.
- **Full System Customization**: The installation process allows you to easily configure system-level settings (like `hostname`, `locale`, network settings) before the first boot.
- **Automatic Disk Partitioning**: OBSCAIS handles disk partitioning according to your specifications in the profile (e.g., GPT, BTRFS), ensuring proper alignment and setup of partitions for maximum efficiency.
- **Minimal External Dependencies**: Runs directly from the Arch live environment with no external dependencies required—perfect for streamlined and portable installation.
- **Post-installation Setup Support**: Includes the ability to run post-installation scripts, providing further customization after the system is set up.
- **Error Prevention with Profile Validation**: The script checks the profile for missing or invalid configurations before starting the installation, preventing issues and ensuring a smooth process.
- **Fast and Efficient**: Optimized for both speed and stability, the script eliminates the need for lengthy manual setup, letting you get your system up and running in no time.

## Requirements

Before using OBSCAIS, ensure your system meets the following requirements:

- **Boot Mode**: UEFI (OBSCAIS does not support legacy BIOS installations).
- **Partitioning Scheme**: GPT.
- **File System**: BTRFS.
- **Internet Connection**: A stable internet connection is required to download packages and sync mirrors during installation.
- **NVMe**: Optimized for use with **NVMe drives**.

## How to Use OBSCAIS

The minimal example profile configuration contains documentation of variables

1. Create a profile according your preferences by copying the "minimal_example" one in "profiles".
2. Install your system with `./obscais.sh install profile_path`.
3. After reboot it's `./obscais.sh setup_root`, reboot then `./obscais.sh setup_user`.


### Commands
- [install profile_path]     # Perform an automated installation using the specified profile.
- [setup_root]               # Configure essential services and set up the root environment.
- [setup_user]               # Finalize the system setup for the user specified in the configuration.
- [clean profile_path]       # Clean and reset the system for a fresh installation using the given profile.
- [check_profile profile_path] # Validate the structure and contents of the provided profile directory.
- [check_internet]           # Verify internet connectivity.
- [help]                     # Display this help message.


-----

### Remember to backup:
- RSS subs from Thunderbird
- Keepass Database

### Creation of an iso image with OBSCAIS integrated and ssh keys for dotfiles cloning:
```bash
sudo pacman -Sy --needed archiso git 7zip
cd $(mktemp -d)
cp -r /usr/share/archiso/configs/releng/ archlive
git clone --depth=1 https://gitlab.com/greyxor/obscais.git archlive/airootfs/root/obscais
# Add your profile into archlive/airootfs/root/obscais/profiles
7z a archlive/airootfs/root/obscais/profiles/laptop/homefs/ssh.7z ~/.ssh -p 
sudo mkarchiso -v archlive
```