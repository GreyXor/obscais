# OBS_CLEAN_EFI: Controls whether EFI boot entries should be cleaned before installation.
# Any other value than "true" will not clean the EFI.
# (including boot order, timeout, and bootnext settings) will be removed before installation.
OBS_CLEAN_EFI="false"

# OBS_DEVICE: Specifies the primary storage device for the Arch Linux installation.
# This device will be partitioned into BOOT, SWAP, ROOT, and HOME partitions.
OBS_DEVICE="/dev/nvme0n1"

# OBS_DEVICE_NVME_FORMAT_LBAF: Determines the Logical Block Addressing Format (LBAF) for the NVMe device.
# Use `nvme id-ns -H /dev/nvme0n1 | grep "Relative Performance"` to view LBAF options.
# Typically, "0" represents 512 bytes/sector, and "1" represents 4K bytes/sector.
# Note: Some features, like `systemd-homed`, might have compatibility issues with 4K sectors.
OBS_DEVICE_NVME_FORMAT_LBAF="0"

# OBS_DEVICE_NVME_FORMAT_SES: Specifies the Secure Erase Settings (SES) for NVMe formatting.
# Defines the type of secure erase operation during formatting (e.g., 0 for no secure erase).
# Check compatibility with `nvme id-ctrl /dev/nvme0n1 -H | grep -E 'Format |Crypto Erase|Sanitize'`.
OBS_DEVICE_NVME_FORMAT_SES="0"

# Partition Sizes: Define the sizes of the partitions created during installation.
# - OBS_DEVICE_PART_BOOT_SIZE: Size of the BOOT partition (e.g., "+1G" for 1 GiB).
# - OBS_DEVICE_PART_SWAP_SIZE: Size of the SWAP partition (e.g., "+2G" for 2 GiB).
# - OBS_DEVICE_PART_ROOT_SIZE: Size of the ROOT partition (e.g., "+20G" for 20 GiB).
# - OBS_DEVICE_PART_HOME_SIZE: Size of the HOME partition (e.g., "0" for the remaining space).
OBS_DEVICE_PART_BOOT_SIZE="+1G"
OBS_DEVICE_PART_SWAP_SIZE="+2G"
OBS_DEVICE_PART_ROOT_SIZE="+20G"
OBS_DEVICE_PART_HOME_SIZE="0"

# OBS_TIMEZONE: Specifies the system timezone to be configured during installation.
# Use `timedatectl list-timezones` to find available options.
OBS_TIMEZONE="Europe/Paris"

# HOMED Settings: Configure the user account created with `systemd-homed`.
# - OBS_HOMED_USERNAME: Username for the new user.
# - OBS_HOMED_USER_GROUPS: Comma-separated list of groups to which the new user should belong, wheel group's already added into user.
OBS_HOMED_USERNAME="example"
OBS_HOMED_USER_GROUPS=""

# OBS_ROOT_SERVICES: Array of system services to be enabled immediately after installation for the root user.
# For example: `OBS_ROOT_SERVICES=(doh-client)`
OBS_ROOT_SERVICES=()

# OBS_PACKAGES_TO_INSTALL: Array of additional packages to be installed during the base system setup.
# Include hardware drivers, tools, or other necessary software.
OBS_PACKAGES_TO_INSTALL=()

# OBS_AUR_PACKAGES: Array of AUR packages to install after setting up the base system.
# Use `setup_user` to install these packages via the AUR helper (e.g., `paru`).
OBS_AUR_PACKAGES=()

# OBS_POSTUSER_SCRIPT: Custom script or commands to execute at the end of `setup_user`.
# Define any additional steps, tweaks, or scripts to be run for further configuration.
OBS_POSTUSER_SCRIPT="
"