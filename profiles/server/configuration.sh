OBS_CLEAN_EFI="true"
OBS_DEVICE="/dev/nvme0n1"
OBS_DEVICE_NVME_FORMAT_LBAF="0"
OBS_DEVICE_NVME_FORMAT_SES="2"
OBS_DEVICE_PART_BOOT_SIZE="+2G"
OBS_DEVICE_PART_SWAP_SIZE="+8G"
OBS_DEVICE_PART_ROOT_SIZE="+150G"
OBS_DEVICE_PART_HOME_SIZE="+750G"
OBS_TIMEZONE="Europe/Paris"
OBS_HOMED_USERNAME="gregoire"
OBS_HOMED_USER_GROUPS=""
OBS_ROOT_SERVICES=(doh-client)
OBS_HARDWARE_PACKAGES=(amd-ucode rocm-opencl-runtime opencl-rusticl-mesa sof-firmware alsa-firmware vulkan-radeon xf86-video-amdgpu)
OBS_KERNEL_PACKAGES=(linux-headers linux-lts linux-lts-headers)
OBS_AUDIO_PACKAGES=(alsa-plugins alsa-utils alsa-tools gst-plugin-pipewire pipewire-alsa pipewire-jack pipewire-pulse pipewire-libcamera pipewire-v4l2 easyeffects calf lsp-plugins-lv2 mda.lv2 yelp zam-plugins-lv2)
OBS_SYSADMIN_PACKAGES=(iptables-nft dns-over-https android-tools power-profiles-daemon wget rsync htop uutils-coreutils moreutils fd ripgrep posix sysstat tcpdump bcc-tools bpftrace trace-cmd docker git-lfs powertop bat eza 7zip linux-docs wireguard-tools dsniff tldr bind whois net-tools openbsd-netcat nmap traceroute iw devtools acpi acpi_call dmidecode clinfo nvme-cli usbutils linux-tools efibootmgr ldns smartmontools)
OBS_MISC_PACKAGES=(vulkan-tools pkgstats)
OBS_PACKAGES_TO_INSTALL=($OBS_HARDWARE_PACKAGES $OBS_KERNEL_PACKAGES $OBS_AUDIO_PACKAGES $OBS_SYSADMIN_PACKAGES $OBS_MISC_PACKAGES)
OBS_AUR_PACKAGES=(ntpd-rs downgrade)
OBS_POSTUSER_SCRIPT="
systemctl disable --now systemd-timesyncd
timedatectl set-ntp false
systemctl enable --now ntpd-rs
sudo ntp-ctl force-sync
sudo hwclock --systohc
"
