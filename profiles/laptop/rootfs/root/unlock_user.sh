#!/bin/zsh

echo "Unlocking user $1"
faillock --user $1 --reset
echo "$1 unlocked"