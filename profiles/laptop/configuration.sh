OBS_CLEAN_EFI="false"
OBS_DEVICE="/dev/nvme0n1"
OBS_DEVICE_NVME_FORMAT_LBAF="1"
OBS_DEVICE_NVME_FORMAT_SES="1"
OBS_DEVICE_PART_BOOT_SIZE="+2G"
OBS_DEVICE_PART_SWAP_SIZE="+8G"
OBS_DEVICE_PART_ROOT_SIZE="+150G"
OBS_DEVICE_PART_HOME_SIZE="0"
OBS_TIMEZONE="Europe/Paris"
OBS_HOMED_USERNAME="gregoire"
OBS_HOMED_USER_GROUPS="video,docker,uucp"
OBS_ROOT_SERVICES=(doh-client bluetooth)
OBS_HARDWARE_PACKAGES=(amd-ucode rocm-hip-runtime rocm-opencl-sdk opencl-rusticl-mesa sof-firmware alsa-firmware vulkan-radeon xf86-video-amdgpu bluez bluez-utils iwd wireless-regdb)
OBS_KERNEL_PACKAGES=(linux-headers linux-hardened linux-hardened-headers linux-lts linux-lts-headers linux-zen linux-zen-headers)
OBS_PIPEWIRE_PACKAGES=(alsa-plugins alsa-utils alsa-tools gst-plugin-pipewire pipewire-alsa pipewire-jack pipewire-pulse pipewire-libcamera gst-plugin-libcamera pipewire-v4l2 easyeffects calf lsp-plugins-lv2 mda.lv2 yelp zam-plugins-lv2)
OBS_FONT_PACKAGES=(inter-font noto-fonts-cjk noto-fonts-emoji noto-fonts-extra ttf-jetbrains-mono)
OBS_SYSADMIN_PACKAGES=(scx-scheds busybox iptables-nft ccache dns-over-https android-tools qemu-desktop virt-manager dnsmasq power-profiles-daemon wget htop uutils-coreutils moreutils fd ripgrep-all posix sysstat tcpdump bcc-tools bpftrace trace-cmd docker git-lfs powertop bat eza 7zip linux-docs wireguard-tools dsniff tldr bind whois net-tools openbsd-netcat nmap traceroute iw devtools acpi acpi_call dmidecode clinfo nvme-cli usbutils linux-tools efibootmgr ldns smartmontools)
OBS_GUI_PACKAGES=(sdl12-compat sdl2-compat kanshi grim brightnessctl pavucontrol foot foot-terminfo i3status-rust wlsunset signal-desktop nm-connection-editor qbittorrent pantheon-polkit-agent gimp keepassxc loupe qalculate-gtk gnome-clocks gnome-disk-utility gnome-firmware gnome-power-manager gnome-sound-recorder gnome-system-monitor qt5-wayland qt6-wayland wtype libreoffice-fresh nautilus mako mpv xdg-desktop-portal-gnome xdg-desktop-portal-kde)
OBS_MISC_PACKAGES=(geoclue sane-airscan mpv-mpris yt-dlp cups-pdf vulkan-tools vulkan-validation-layers pkgstats)
OBS_PACKAGES_TO_INSTALL=($OBS_HARDWARE_PACKAGES $OBS_KERNEL_PACKAGES $OBS_PIPEWIRE_PACKAGES $OBS_FONT_PACKAGES $OBS_SYSADMIN_PACKAGES $OBS_GUI_PACKAGES $OBS_MISC_PACKAGES)
OBS_AUR_SWAY=(libliftoff-git wlroots-git sway-git swayidle-git swaylock-git swaybg-git xdg-desktop-portal-wlr-git sway-systemd sway-launcher-desktop-git)
OBS_AUR_PACKAGES=($OBS_AUR_SWAY automatic-timezoned ntpd-rs pwvucontrol framework-system-git fw-ectool-git bibata-cursor-theme-bin downgrade zsh-antidote firefox-nightly-bin thunderbird-nightly-bin vscodium-bin)
OBS_POSTUSER_SCRIPT="
7z x ssh.7z
rm -f ssh.7z

systemctl disable --now systemd-timesyncd
timedatectl set-ntp false
systemctl enable --now ntpd-rs
sudo ntp-ctl force-sync
sudo hwclock --systohc

rm -rf .ssh/known_hos*
chmod 700 .ssh/
chmod 600 .ssh/*
chmod 644 .ssh/*.pub

git init
git remote add origin git@gitlab.com:greyxor/dotfiles.git
git fetch
git checkout -f main

git clone --depth=1 git@gitlab.com:greyxor/aur-font-awesome-pro-nulled.git /tmp/aur-font-awesome-pro-nulled
cd /tmp/aur-font-awesome-pro-nulled
makepkg -si --noconfirm
"